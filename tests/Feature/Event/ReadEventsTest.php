<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReadEventsTestTest extends TestCase
{
    use DatabaseMigrations;

    protected $event;

    public function setUp()
    {
        parent::setUp();

        $this->event = create('App\Acme\Event');
    }

    /** @test */
    public function a_user_can_view_all_events()
    {
        $this->get('/events')
            ->assertSee($this->event->title);
    }

    /** @test */
    public function a_user_can_read_a_single_event()
    {
        $this->get($this->event->path())
            ->assertSee($this->event->title);
    }
}
