<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeleteEventTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_thread_can_be_deleted()
    {
        // $this->signIn();

        $event = create('App\Acme\Event');

        // $this->json('DELETE', $event->path());
        $this->delete($event->path());

        $this->assertDatabaseMissing('events', ['id' => $event->id]);
    }
}
