<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_has_a_creator()
    {
        $event = create('App\Acme\Event');

        $this->assertInstanceOf('App\Acme\User', $event->creator);
    }

    /** @test */
    // public function it_can_add_a_reply()
    // {
    //     $this->thread->addReply([
    //         'body'    => 'Foobar',
    //         'user_id' => 1
    //     ]);

    //     $this->assertCount(1, $this->thread->replies);
    // }
}
