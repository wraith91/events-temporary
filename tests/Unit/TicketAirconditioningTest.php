<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TicketAirconditioningTest extends TestCase
{
    use DatabaseMigrations;

    protected $ac;

    public function setUp()
    {
        parent::setUp();

        $this->ac = create('App\Acme\TicketAirconditioning');
    }

    /** @test */
    public function it_is_ticketable()
    {
        $this->ac->createTicket(['body' => 'Hello']);

        $this->assertCount(1, $this->ac->ticket);
    }

    /** @test */
    public function it_has_a_creator()
    {
        $this->assertInstanceOf('App\Acme\User', $this->ac->creator);
    }
}
