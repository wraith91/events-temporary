<?php

use App\Acme\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            ['name' => 'Airconditioning'],
            ['name' => 'Borrowing of Equipment'],
            ['name' => 'CCTV Footage'],
            ['name' => 'Gatepass'],
            ['name' => 'Housekeeping'],
            ['name' => 'Locker'],
            ['name' => 'Logistics'],
            ['name' => 'Pantry Reservation'],
            ['name' => 'Music Room Reservation'],
            ['name' => 'Repairs and Maintenance'],
            ['name' => 'Office Furniture and Fixture'],
            ['name' => 'Shuttle Request'],
            ['name' => 'Work Permit']
        ]);
    }
}
