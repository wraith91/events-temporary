let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('resources/assets/components/fullcalendar/dist/fullcalendar.min.js', 'public/js')
	.copy('resources/assets/components/jquery/dist/jquery.min.js', 'public/js')
    .copy('resources/assets/components/moment/min/moment.min.js', 'public/js')
	.copy('resources/assets/image', 'public/images/', false)
    .copy('resources/assets/components/fullcalendar/dist/fullcalendar.min.css', 'public/css');

   // .js('resources/assets/js/app.js', 'public/js')
   // .sass('resources/assets/sass/app.scss', 'public/css');
