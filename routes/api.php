<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/events', function () {

    $events = App\Acme\Event::select('id', 'user_id', 'title', 'start_time as start', 'end_time as end')->get();

    foreach ($events as $event) {
        // $event->title = $event->title . ' - ' .$event->creator->name;
        $event->title = $event->title;
        $event->url = url('events/' . $event->id);
    }

    return $events;

});
