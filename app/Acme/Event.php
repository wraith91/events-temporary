<?php

namespace App\Acme;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['start_time', 'end_time'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function path()
    {
        return '/events/' . $this->id;
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getDurationAttribute()
    {
        if ($this->end_time->diffInMinutes($this->start_time) >= 120) {
            return $this->end_time->diffInHours($this->start_time) . 'hrs';
        }

        return $this->end_time->diffInMinutes($this->start_time) . ' mins';
    }

}
