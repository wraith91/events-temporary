<?php

namespace App\Acme;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    /**
     * Fillable fields for the model
     *
     * @var array
     */
    protected $fillable = ['body'];

    /**
     * Get all of the owning ticketable models.
     */
    public function ticketable()
    {
        return $this->morphTo();
    }

}
