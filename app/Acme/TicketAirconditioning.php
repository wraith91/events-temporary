<?php

namespace App\Acme;

use App\Acme\Ticket;
use Illuminate\Database\Eloquent\Model;

class TicketAirconditioning extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get all of the tickets for airconditioning
     */
    public function ticket()
    {
        return $this->morphMany(Ticket::class, 'ticketable');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function createTicket($ac)
    {
        return $this->ticket()->create($ac);
    }
}
