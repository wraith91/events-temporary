@extends('layouts.app')

@section('content')
<div class="container">

{{--    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li class="active">You are here: Home</li>
            </ol>
        </div>
    </div> --}}

    <div class="row">
        <div class="col-lg-12">
            <h1 class="display-2">Events</h1>
        </div>
    </div>

{{--     <div class="row">
        <div class="col-md-12">
            <div id='calendar'>
                <table class="table">
                  <thead class="thead-inverse">
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Title</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                      <th>Duration</th>
                      <th>When</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($events as $i => $event)
                        <tr>
                          <th scope="row">{{ $i+1 }}</th>
                          <td>{{ $event->creator->name }}</td>
                          <td>
                            <a href="{{ $event->path() }}">{{ $event->title }}</a>
                          </td>
                          <td>{{ $event->start_time->format('M d, Y g:i A') }}</td>
                          <td>{{ $event->end_time->format('M d, Y g:i A') }}</td>
                          <td>{{ $event->duration }}</td>
                          <td>{{ $event->start_time->diffForHumans() }}</td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
        </div>
    </div>
 --}}
    <div class="row">
        <div class="col-md-12">
            <div id='calendar'>
            </div>
        </div>
    </div>

</div>

@endsection

@section('footer')
    <script type="text/javascript">
        $(document).ready(function() {

            var base_url = '{{ url('/') }}';
            $('#calendar').fullCalendar({
                weekends: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                events: {
                    url: '/api/events',
                    error: function() {
                        alert("There's something wrong with the API.");
                    }
                }
            });
        });
    </script>
@endsection
