@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="display-2">Event: <strong>{{ $event->title }}</strong></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div id='calendar'>
                <table class="table">
                  <thead class="thead-inverse">
                    <tr>
                      <th>Name</th>
                      <th>Title</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{{ $event->creator->name }}</td>
                      <td>
                        <a href="{{ $event->path() }}">{{ $event->title }}</a>
                      </td>
                      <td>{{ $event->start_time->format('M d, Y g:i A') }}</td>
                      <td>{{ $event->end_time->format('M d, Y g:i A') }}</td>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection
