<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <!-- Styles -->
        <style>
            strong {
                font-weight: bold;
            }

            tr, td {
                padding: 5px 10px;
                word-wrap: break-word;
                width: 15px;
            }

            td {
                height: 18px;
            }

            td.mid-pad {
                height: 48px;
                vertical-align: middle;
            }

            .border-bottom {
                border-bottom: 1px solid #000000;
            }

            .stripped, .stripped td {
                border: 1px solid #000000;
            }

            .dashed {
                border-bottom: 1px dashed #000000;
            }

            .align-left {
                text-align: left;
            }

            .align-center {
                text-align: center;
            }

            .align-right {
                text-align: right;
            }

            .custom-table {
                /*width: 100%;*/
                table-layout: fixed;
                border-collapse: collapse;
                margin: 0 10px;
            }

        </style>
    </head>
    <body>
        <table class="custom-table">
            <tr>
                <td colspan="6" class="align-right">Admin's Copy</td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <img src="images/mckinley_logo_64.png" height="47" width="64" alt="">
                </td>
            </tr>
            <tr>
                <td colspan="6" class="align-center mid-pad">
                    2WS Building Administration Inc., <br>
                    #22 Upper McKinley Road, McKinley Town Center <br>
                    Fort Bonifacio, Taguig City
                </td>
            </tr>
            <tr>
                <td colspan="6" class="align-center">
                    <strong>AIR-CON EXTENSION REQUEST</strong>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="align-left">
                    <strong>Tenant/Floor:</strong> 2W15th Floor
                </td>
                <td colspan="3" class="align-right">
                    <strong>Date:</strong> 08/25/2017
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr class="stripped">
                <td colspan="2"><strong>Date</strong></td>
                <td colspan="2"><strong>Time</strong></td>
                <td colspan="2"><strong>Remarks</strong></td>
            </tr>
            <tr>
                <td class="stripped"><strong>From</strong></td>
                <td class="stripped"><strong>To</strong></td>
                <td class="stripped"><strong>From</strong></td>
                <td class="stripped"><strong>To</strong></td>
                <td class="stripped"></td>
                <td class="stripped"></td>
            </tr>
            <tr>
                <td class="stripped" colspan="1">1</td>
                <td class="stripped" colspan="1">2</td>
                <td class="stripped" colspan="1">3</td>
                <td class="stripped" colspan="1">4</td>
                <td class="stripped" colspan="2">5</td>
            </tr>
            <tr>
                <td class="stripped" colspan="1">1</td>
                <td class="stripped" colspan="1">2</td>
                <td class="stripped" colspan="1">3</td>
                <td class="stripped" colspan="1">4</td>
                <td class="stripped" colspan="2">5</td>
            </tr>
            <tr>
                <td class="stripped" colspan="1">1</td>
                <td class="stripped" colspan="1">2</td>
                <td class="stripped" colspan="1">3</td>
                <td class="stripped" colspan="1">4</td>
                <td class="stripped" colspan="2">5</td>
            </tr>
            <tr><td></td></tr>
            <tr>
                <td colspan="2" class="align-left">Requested By:</td>
                <td colspan="2" class="align-left">Approved By:</td>
                <td colspan="2" class="align-left">Noted By:</td>
            </tr>
            <tr>
                <td colspan="2" class="align-left">
                    lorem
                </td>
                <td colspan="2" class="align-left">
                    lorem
                </td>
                <td colspan="2" class="align-left">
                    lorem
                </td>
            </tr>
            <tr>
                <td colspan="2" class="align-left">Authorized Representative</td>
                <td colspan="2" class="align-left">Property Engineer</td>
                <td colspan="2" class="align-left">Property Manager</td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td colspan="6" class="align-left">Note:</td>
            </tr>
            <tr>
                <td colspan="6" class="align-left">*Regular Air-Con Operation is first 8 hours Monday to Friday</td>
            </tr>
            <tr>
                <td colspan="6" class="align-left">*Saturday is halfday only (8:30 AM - 12:30 NN)</td>
            </tr>
            <tr>
                <td colspan="6" class="dashed"></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td colspan="6" class="align-right">Requestor's Copy</td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <img src="images/mckinley_logo_64.png" height="47" width="64" alt="">
                </td>
            </tr>
            <tr>
                <td colspan="6" class="align-center mid-pad">
                    2WS Building Administration Inc., <br>
                    #22 Upper McKinley Road, McKinley Town Center <br>
                    Fort Bonifacio, Taguig City
                </td>
            </tr>
            <tr>
                <td colspan="6" class="align-center">
                    <strong>AIR-CON EXTENSION REQUEST</strong>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="align-left">
                    <strong>Tenant/Floor:</strong> 2W15th Floor
                </td>
                <td colspan="3" class="align-right">
                    <strong>Date:</strong> 08/25/2017
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr class="stripped">
                <td colspan="2"><strong>Date</strong></td>
                <td colspan="2"><strong>Time</strong></td>
                <td colspan="2"><strong>Remarks</strong></td>
            </tr>
            <tr>
                <td class="stripped"><strong>From</strong></td>
                <td class="stripped"><strong>To</strong></td>
                <td class="stripped"><strong>From</strong></td>
                <td class="stripped"><strong>To</strong></td>
                <td class="stripped"></td>
                <td class="stripped"></td>
            </tr>
            <tr>
                <td class="stripped" colspan="1">1</td>
                <td class="stripped" colspan="1">2</td>
                <td class="stripped" colspan="1">3</td>
                <td class="stripped" colspan="1">4</td>
                <td class="stripped" colspan="2">5</td>
            </tr>
            <tr>
                <td class="stripped" colspan="1">1</td>
                <td class="stripped" colspan="1">2</td>
                <td class="stripped" colspan="1">3</td>
                <td class="stripped" colspan="1">4</td>
                <td class="stripped" colspan="2">5</td>
            </tr>
            <tr>
                <td class="stripped" colspan="1">1</td>
                <td class="stripped" colspan="1">2</td>
                <td class="stripped" colspan="1">3</td>
                <td class="stripped" colspan="1">4</td>
                <td class="stripped" colspan="2">5</td>
            </tr>
            <tr><td></td></tr>
            <tr>
                <td colspan="2" class="align-left">Requested By:</td>
                <td colspan="2" class="align-left">Approved By:</td>
                <td colspan="2" class="align-left">Noted By:</td>
            </tr>
            <tr>
                <td colspan="2" class="align-left">
                    lorem
                </td>
                <td colspan="2" class="align-left">
                    lorem
                </td>
                <td colspan="2" class="align-left">
                    lorem
                </td>
            </tr>
            <tr>
                <td colspan="2" class="align-left">Authorized Representative</td>
                <td colspan="2" class="align-left">Property Engineer</td>
                <td colspan="2" class="align-left">Property Manager</td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td colspan="6" class="align-left">Note:</td>
            </tr>
            <tr>
                <td colspan="6" class="align-left">*Regular Air-Con Operation is first 8 hours Monday to Friday</td>
            </tr>
            <tr>
                <td colspan="6" class="align-left">*Saturday is halfday only (8:30 AM - 12:30 NN)</td>
            </tr>
        </table>
    </body>
</html>
